from django.db import models
# from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)

class Shoe(models.Model):
    """
    The Shoe model englobes the following attributes : Model, Color, Manufacturer, and a Picture URL.
    """

    model = models.CharField(max_length=40)
    color = models.CharField(max_length=40)
    manufacturer = models.CharField(max_length=40)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True,
    )

    # def get_api_url(self):
    #     return reverse("api_list_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return self.model

    class Meta:
        ordering = ("id",)  # Default ordering for Shoe
