# Wardrobify

Team:

* Paul-Adrien EPINETTE - Shoes
* Joshua Contreras - Hats

## Design

<img src="ghi/app/src/Microservice-2shots-context.map">
[Context Map](https://excalidraw.com/#json=OcWZ59wiqW64hf1mYb867,gX7NGPKR-fl8P8i513G7rQ)

## Shoes microservice

There are two models in the Shoes microservice: `BinVO` and `Shoe`.
These models aare used to represent bins and shoes.

    1. `BinVO` model:
        - The `BinVO` model represents a bin in the wardrobe
        - It consists of the following properties:
            - `import_href`: A unique identifier for the bin (URL to the bin)
            - `closet_name`: The name of the closet where the bin is located
            - `bin_number`: The number of the bin
            - `bin_size`: The size of the bin
        - To ensure each bin has a unique identifier, the `import_href` is set as `unique=true`
        - To allow the Shoes microservice to retieve and update bin information from the wardrobe microservice, the `BinVO` model is integrated through the `import_href` field.

    2. `Shoe` model:
        - The `Shoe` model represents a shoe
        - It consists of the following properties:
            - `model`: The model of the shoe
            - `color`: The color of the shoe
            - `manufcaturer`: The manufcaturer of the shoe
            - `picture_url`: The picture URL of the shoe
            - `bin`: A foreign key relationship with the `BinVO` model to show in which bin the shoe in stored
        - For the Shoes microservice to link a shoe with a specific bin from the wardrobe, the `Shoe` model is integrated with the wardrobe microservice by storing the `import_href` of the bin in the `bin` field

The integration between the shoes microservice and the wardrobe microservice is made possible thanks to the `import_href` field of the `BinVO` model.

This field is used as a unique identifier for bins and allows the Shoes microservice to communicate with the wardrobe microservice by fetching bin information based on the `import_href`.

This integration allows the Shoes microservice to link shoes with specific bins in the wardrobe and access bin details when needed.

## Hats microservice
There are two models in the Hats microservice: LocationVO and Hat.

    LocationVO is a location in the wardrobe where the hats can be placed.
    The unique identifier has import_href(unique id), closet_name (name of the closet), section_number (section in the closet) and a shelf_number(specific shelf within a section)

    Hat are the objects being stored. There are different characteristics for these hats: name,
    fabric, style, color, and the picture_url. It also uses LocationVO with a foreign key as a characteristic.

Hat objects is integrated using location as the value object that is polled from wardrobe API. LocationVO is used as the unique identifier for all the details of each hat. We use react to fetch the data within HatList to display HatForm.
