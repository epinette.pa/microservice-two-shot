import React, { useState } from 'react';

function ShoeForm({ bins, loadShoes } ) {
  const [model, setModel] = useState('');
  const [color, setColor] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [picture_url, setPicture_Url] = useState('');
  const [bin, setBin] = useState('');

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      model,
      color,
      manufacturer,
      picture_url,
      bin,
    };


    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);
      loadShoes()

      setModel('');
      setColor('');
      setManufacturer('');
      setPicture_Url('');
      setBin('');
    }
  }

  function handleChangeModel(event) {
    const value = event.target.value;
    setModel(value);
  }

  function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleChangeManufacturer(event) {
    const value = event.target.value;
    setManufacturer(value);
  }

  function handleChangePicture_Url(event) {
    const value = event.target.value;
    setPicture_Url(value);
  }

  function handleChangeBin(event) {
    const value = event.target.value;
    setBin(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeModel} value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="name">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="starts">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeManufacturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufaturer" id="manufacturer" className="form-control" />
              <label htmlFor="ends">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangePicture_Url} value={picture_url} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="ends">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeBin} value={bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
