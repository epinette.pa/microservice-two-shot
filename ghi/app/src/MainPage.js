function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
    </div>
  );
}
export default MainPage;


// import React, { useState, useEffect } from 'react';
// // import { Link } from 'react-router-dom';

// function BinColumn({bins}) {

//   return (
//     <div className="col">
//       {bins.map(bin => {
//         console.log("Closet name", bin.closet_name)
//         return (
//           <div key={bin.href} className="card mb-3 shadow">
//             <div className="card-body">
//               <h5 className="card-title">{bin.closet_name}</h5>
//               <h6 className="card-subtitle mb-2 text-muted">
//                 {bin.bin_number}
//               </h6>
//               <p className="card-text">
//                 {bin.bin_size}
//               </p>
//             </div>
//           </div>
//         );
//       })}
//     </div>
//   );
// }


// function MainPage({ bins }) {
//   const [binColumns, setbinColumns] = useState([]);

//   useEffect(() => {
//     async function getBinDetails() {
//       try {
//         const requests = [];
//         for (let bin of bins) {
//           const detailUrl = `http://localhost:8080${bin.href}shoes`;
//           requests.push(fetch(detailUrl));
//         }
//         const responses = await Promise.all(requests);

//         const binColumns = [[], [], []];

//         let i = 0;
//           for (const binResponse of responses) {
//             if (binResponse.ok) {
//               const details = await binResponse.json();
//               binColumns[i].push(details);
//               i = i + 1;
//               if (i > 2) {
//                 i = 0;
//               }
//             } else {
//               console.error(binResponse);
//             }
//           }

//           setbinColumns(binColumns);
//       } catch (error) {
//         console.error(error);
//       }
//     }
//     getBinDetails();
// }, [bins])

//   return (
//     <>
//       <div className="px-4 py-5 my-5 text-center">
//         <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
//         <div className="col-lg-6 mx-auto">
//           <p className="lead mb-4">
//             Need to keep track of your shoes and hats? We have
//             the solution for you!
//           </p>
//         </div>
//       </div>
//       <div className="container">
//       <h2>Bins</h2>
//       <div className="row">
//         {binColumns.map((binList, index) => {
//           return (
//             <BinColumn key={index} bins={binList} />
//           );
//         })}
//       </div>
//       </div>
//     </>
//   );
// }

// export default MainPage;
