import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useEffect, useState } from 'react';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import HatsList from './HatsList';

function App(props) {


  const [ shoes, setShoes ] = useState([]);
  const [ bins, setBins ] = useState([]);

  async function getBins() {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
      console.log("bins", data.bins)
    }
  }

  async function loadShoes() {
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
      console.log("shoes", data.shoes)
    } else {
      console.error(response);
    }
  }

  const handleDelete = async (shoeId) => {
    const url = `http://localhost:8080/api/shoes/${shoeId}`;
    const response = await fetch(url, {
      method: "DELETE",
    });

    if (response.ok) {
      setShoes(shoes.filter((shoe) => shoe.id !== shoeId));
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    getBins();
    loadShoes();
    console.log('Use effect fired!')
  }, []);

  if (props.hats === undefined) {
    return null
  }

  if (shoes === undefined) {
    return null;
  }
  console.log('React rendered');
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatsList hats={props.hats} /> } />
            <Route path="new" element={<HatForm /> } />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoes} onDelete={handleDelete}/>} />
            <Route path="new" element={<ShoeForm loadShoes={loadShoes} bins={bins} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
