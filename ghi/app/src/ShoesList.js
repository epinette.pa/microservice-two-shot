import { NavLink } from "react-router-dom";

function ShoesList({ shoes, onDelete }) {

  const handleDelete = (id) => {
    onDelete(id);
  }

    return (
      <div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Model Name</th>
              <th>Manufacturer</th>
              <th>Color</th>
              <th>Bin Info</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.model }</td>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.color }</td>
                  <td>{ `${shoe.bin.closet_name} ${shoe.bin.bin_number} ${shoe.bin.bin_size}` }</td>
                  <td>
                    <button onClick={() => handleDelete(shoe.id)}>DELETE</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="mb-3">
          <NavLink to="/shoes/new" className="btn btn-primary mb-3">Add a new Shoe</NavLink>
        </div>
      </div>
    );
  }

  export default ShoesList;
